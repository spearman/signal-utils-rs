//! Sequence builder

use derive_builder::Builder;

#[derive(Builder)]
pub struct Sequence <T : Copy + std::ops::Add <Output=T>> {
  start:    T,
  interval: T
}
pub type Builder <T> = SequenceBuilder <T>;

impl <T : Copy + std::ops::Add <Output=T>> Iterator for Sequence <T> {
  type Item = T;
  fn next (&mut self) -> Option <T> {
    let out = Some (self.start);
    self.start = self.start + self.interval;
    out
  }
}
