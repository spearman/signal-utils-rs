//! Signal processing utilities

pub mod generator;
pub mod pcm;
pub mod sequence;

pub use self::generator::Generator;
pub use self::sequence::Sequence;
