use signal_utils::*;

macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e) }
}

fn main() {
  println!("signal-utils example: main...");
  sequence::Builder::default().start (0).interval (10).build().unwrap()
    .take (10).enumerate().for_each (|i| show!(i));
  println!("signal-utils example: ...main");
}
